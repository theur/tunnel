package main

import (
	"flag"
	"fmt"
	"github.com/elazarl/goproxy"
	"log"
	"net/http"
	"strings"
)

func main() {
	addrPtr := flag.String("addr", "127.0.0.1:4239", "bind address")
	quietPtr := flag.Bool("quiet", false, "quiet mode (hide traffic log)")
	flag.Parse()
	fmt.Printf("Binding to address: %s\n", *addrPtr)
	fmt.Printf("Quiet mode:         %v\n", *quietPtr)
	isLocal := (strings.Contains(*addrPtr, "127.0.0.1") || strings.Contains(*addrPtr, "localhost"))
	if !isLocal {
		fmt.Println("Warning: not bound to loopback interface")
	}
	proxy := goproxy.NewProxyHttpServer()
	proxy.Verbose = !(*quietPtr)
	fmt.Println("\nReady to serve traffic.")
	log.Fatal(http.ListenAndServe(*addrPtr, proxy))
}
