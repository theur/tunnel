# tunnel

Minimal HTTP proxy written in Go.


## Releases

See https://gitlab.com/theur/tunnel/tags for pre-compiled releases.
